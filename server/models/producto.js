const mongoose = require('mongoose');
const { Schema } = mongoose;

const productoSchema = new Schema( {
    id: { type: Number , required: true},
    name: { type: String, required: true},
    type: { type: String, required: true},
    status: { type: String, required: true},
    featured: { type: Boolean, required: true},
    description: { type: String, required: true},
    short_description: { type: String, required: false},
    sku: { type: String, required: false},
    price: { type: String, required: true},
    regular_price: { type: String, required: false},
    sale_price: { type: String, required: false},
   // total_sales: { type: Number, required: false},
  //  tax_status: { type: String, required: true},
  //  tax_class: { type: String, required: true},
    manage_stock: { type: Boolean, required: false},
    stock_quantity: { type: Number, required: false},
    stock_status: { type: String, required: false},
    parent_id: { type: Number, required: false},
    categories:  { any: Schema.Types.Mixed },
    tags: { any: Schema.Types.Mixed},
    images: { any: Schema.Types.Mixed},
    attributes: { any: Schema.Types.Mixed},
    variations: { any: Schema.Types.Mixed},
    variationsProducts:{ any: Schema.Types.Mixed }
});

module.exports = mongoose.model('producto', productoSchema);
