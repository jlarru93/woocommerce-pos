const product = require('../models/product');
//const woocommerce=require('../controllers/woocommerce.controller');
const productCtrl = {};

productCtrl.getproducts = async (req, res, next) => {
    const products = await product.find();
    res.json(products);
};

productCtrl.createproduct = async (req, res, next) => {
    const product = new product({
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    });
    await product.save();
    res.json({status: 'product created'});
};

productCtrl.getproduct = async (req, res, next) => {
    const { id } = req.params;
    const product = await product.findById(id);
    res.json(product);
};

productCtrl.editproduct = async (req, res, next) => {
    const { id } = req.params;
    const product = {
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary
    };
    await product.findByIdAndUpdate(id, {$set: product}, {new: true});
    res.json({status: 'product Updated'});
};
productCtrl.deleteproduct = async (req, res, next) => {
    await product.findByIdAndRemove(req.params.id);
    res.json({status: 'product Deleted'});
};

module.exports = productCtrl;