//const product = require('../models/product');
const woocommerceCtrl = {};
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
const producto = require('../models/producto');

var WooCommerce = new WooCommerceRestApi({
  url: 'https://woocommerce.sytes.net/',
  consumerKey: 'ck_5fd0457a71ee7b76aa72dbcfd2040f808be8c1fb',
  consumerSecret: 'cs_de8a115bf7b6a4e231cc0d153a148decaeabcb8c',
 // wpAPI: true,
  version: 'wc/v3'
});

woocommerceCtrl.sincronizar= async (req, res, next) => {
  var productosSimpleResp=await WooCommerce.get('products?type=simple');
  var productosSimple=productosSimpleResp.data;
  for(i=0;i<productosSimple.length;i++){
    let product=productosSimple[0];
    if(product.stock_quantity==0){
      productosSimple.splice(i, 1);      
    }    
  }
  
  var productosvariableResp=await WooCommerce.get('products?type=variable');
  var productosvariable=productosvariableResp.data;
  for(i=0;i<productosvariable.length;i++){
    
    var productVariablesResp=await WooCommerce.get("products/"+productosvariable[i].id+"/variations");
    var productVariables=productVariablesResp.data
    for(j=0;j<productVariables.length;j++){
      if(productVariables[j].stock_quantity==null || productVariables[j].stock_quantity==0){
        productVariables.splice(j, 1);
      }
    }

    productosvariable[i].variationsProducts=productVariables;
  }
  var allProduct=productosSimple.concat(productosvariable);

  for (let index = 0; index < allProduct.length; index++) {
    const prodOfAll = allProduct[index];
    const prod=new producto({
      id: prodOfAll.id,
      name: prodOfAll.name,
      type: prodOfAll.type,
      status: prodOfAll.status,
      featured: prodOfAll.featured,
      description: prodOfAll.description,
      short_description: prodOfAll.short_description,
      sku: prodOfAll.sku,
      price: prodOfAll.price,
      regular_price: prodOfAll.regular_price,
      sale_price: prodOfAll.sale_price,
      total_sales: prodOfAll.total_sales,
      tax_status: prodOfAll.tax_status,
      tax_class: prodOfAll.tax_class,
      manage_stock: prodOfAll.manage_stock,
      stock_quantity: prodOfAll.stock_quantity,
      stock_status: prodOfAll.stock_status,
      parent_id: prodOfAll.parent_id,
      categories:  prodOfAll.categories,
      tags: prodOfAll.tags,
      images: prodOfAll.images,
      attributes: prodOfAll.attributes,
      variations: prodOfAll.variations,
      variationsProducts:prodOfAll.variationsProducts
  });
  await prod.save();
  }

  res.status(200).send(allProduct);
};

woocommerceCtrl.vender=async (req, res, next)=>{
  const data = {
    payment_method: "cod",
    payment_method_title: "Contra reembolso",
    set_paid: true,
    billing: {
      first_name: "jessy",
      last_name: "Davila",
      address_1: "pisana",
      address_2: "",
      city: "San Francisco",
      state: "CA",
      postcode: "94103",
      country: "PE",
      email: "john.doe@example.com",
      phone: "(555) 555-5555"
    },
  /*  shipping: {
      first_name: "John",
      last_name: "Doe",
      address_1: "969 Market",
      address_2: "",
      city: "San Francisco",
      state: "CA",
      postcode: "94103",
      country: "US"
    },*/
    line_items: [
      {
        product_id: 84,
        total: "100",
        quantity: 2
      },
      {
        product_id: 655,
        variation_id: 660,
        total: "150",
        quantity: 1
      }
    ]/*,
    shipping_lines: [
      {
        method_id: "flat_rate",
        method_title: "Flat Rate",
        total: 10
      }
    ]*/
  };
  
  WooCommerce.post("orders", data)
    .then((response) => {
      console.log(response.data);
      res.status(200).send(response.data);
    })
    .catch((error) => {
      console.log(error.response.data);
      res.status(200).send(error.response.data);
    });
}


module.exports = woocommerceCtrl;