const express = require('express');
const cors = require('cors');
const app = express();
const employeeRouter=require('./routes/employee.routes');
const productRouter=require('./routes/product.routes');
const woocommerceRouter=require('./routes/woocommerce.routes');
const { mongoose } = require('./database');

// Settings
app.set('port', process.env.PORT || 3000);

// Middlewares
app.use(cors({origin: '*'}));
app.use(express.json());

// Routes
app.use('/api/employees', employeeRouter);
app.use('/api/product', productRouter);
app.use('/api/woocommerce', woocommerceRouter);

// starting the server
app.listen(app.get('port'), () => {
    console.log(`server on port ${app.get('port')}`);
});