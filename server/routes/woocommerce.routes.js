
const express = require('express');
const router = express.Router();

const woocommerce = require('../controllers/woocommerce.controller');
router.get('/', woocommerce.sincronizar);
router.get('/vender', woocommerce.vender);
module.exports = router;