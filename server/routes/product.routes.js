const express = require('express');
const router = express.Router();

const product = require('../controllers/product.controller');

router.get('/', product.getproducts);
router.post('/', product.createproduct);
router.get('/:id', product.getproduct);
router.put('/:id', product.editproduct);
router.delete('/:id', product.deleteproduct);

module.exports = router;